import React from 'react'

import useWordGame from './useWordGame'
import './App.css'

const App = () => {
	const {
		inputRef,
		handleChange,
		text,
		isTimeRunning,
		timer,
		startGame,
		wordCount,
	} = useWordGame()

	return (
		<div>
			<h1>Title</h1>
			<textarea
				ref={inputRef}
				onChange={handleChange}
				value={text}
				disabled={!isTimeRunning}
			/>
			<h4>Time remaining: {timer}</h4>
			<button onClick={startGame} disabled={isTimeRunning}>
				Start Game
			</button>
			<h1>Word count: {wordCount(text)}</h1>
		</div>
	)
}

export default App
