import { useState, useEffect, useRef } from 'react'

const useWordGame = () => {
	const GAME_LENGTH = 5
	const [text, setText] = useState('')
	const [timer, setTimer] = useState(GAME_LENGTH)
	const [isTimeRunning, setIsTimeRunning] = useState(false)
	const inputRef = useRef(null)

	const handleChange = (event) => {
		const { value } = event.target
		setText(value)
	}

	const wordCount = (words) => {
		if (timer === 0 && isTimeRunning === false) {
			return words.split(' ').filter((words) => words !== '').length
		}
	}

	useEffect(() => {
		if (isTimeRunning && timer > 0) {
			setTimeout(() => setTimer(timer - 1), 1000)
		} else if (timer === 0) {
			endGame()
		}
	}, [timer, isTimeRunning])

	const startGame = () => {
		setIsTimeRunning(true)
		setText('')
		setTimer(GAME_LENGTH)
		inputRef.current.disabled = false
		inputRef.current.focus()
	}

	const endGame = () => {
		setIsTimeRunning(false)
	}

	return {
		inputRef,
		handleChange,
		text,
		isTimeRunning,
		timer,
		startGame,
		wordCount,
	}
}

export default useWordGame
